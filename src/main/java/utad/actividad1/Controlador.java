package utad.actividad1;

import android.content.Intent;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;

/**
 * Created by alexander.sari on 08/11/2016.
 */
public class Controlador implements View.OnClickListener, View.OnFocusChangeListener{

    MainActivity vista;
    public boolean editar;
    public boolean cancelar;
    public Controlador(MainActivity vista){
        this.vista = vista;
    }

    @Override

            //

    public void onClick(View view) {

        if (view.getId() == vista.btnEditar.getId()) {

            if(editar){
                vista.btnVolver.setText("VOLVER");
                vista.btnEditar.setText("EDITAR");
                vista.txtNombre.setEnabled(false);
                vista.txtEmail.setEnabled(false);
                vista.txtTelf.setEnabled(false);
                vista.txtDirec.setEnabled(false);
                editar = false;
                DataHolder.sNombre = vista.txtNombre.getText().toString();
                DataHolder.sEmail = vista.txtEmail.getText().toString();
                DataHolder.sTelf = vista.txtTelf.getText().toString();
                DataHolder.sDirec = vista.txtDirec.getText().toString();

            }
            else{
                vista.btnVolver.setText("CANCELAR");
                vista.btnEditar.setText("GUARDAR");
                vista.txtNombre.setEnabled(true);
                vista.txtEmail.setEnabled(true);
                vista.txtTelf.setEnabled(true);
                vista.txtDirec.setEnabled(true);
                editar = true;

            }

        }else if(view.getId() == vista.btnVolver.getId()){

            if(editar){
                vista.txtNombre.setText("");
                vista.txtEmail.setText("");
                vista.txtTelf.setText("");
                vista.txtDirec.setText("");
                vista.btnVolver.setText("VOLVER");
                vista.btnEditar.setText("EDITAR");
                vista.txtNombre.setEnabled(false);
                vista.txtEmail.setEnabled(false);
                vista.txtTelf.setEnabled(false);
                vista.txtDirec.setEnabled(false);
                editar = false;
            }else{
                Intent intent = new Intent(vista, SegundoActivity.class);
                vista.startActivity(intent);
                vista.finish();
            }



        }

    }

    @Override
    public void onFocusChange(View view, boolean b) {

    }
}
