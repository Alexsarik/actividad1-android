package utad.actividad1;

/**
 * Created by alexander.sari on 08/11/2016.
 */


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SegundoActivity extends AppCompatActivity {
    public SegundoActivity SegundoActivity;
    public Button btnPerfil;
    public EditText txtTitulo;
    public TextView txtCont;
    public Button btnSig;
    public Button btnAtras;

    @Override
    public WindowManager getWindowManager(){return super.getWindowManager();}
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.segundo_activity);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        btnPerfil =(Button) this.findViewById(R.id.btnPerfil);
        btnSig =(Button) this.findViewById(R.id.siguiente);
        btnAtras =(Button) this.findViewById(R.id.btnAtras);
        txtTitulo = (EditText) this.findViewById(R.id.editText_capitulos);
        txtCont = (TextView) this.findViewById(R.id.txtCont);

        Controlador2 Controlador2 = new Controlador2(this);

        btnPerfil.setOnClickListener(Controlador2);
        btnSig.setOnClickListener(Controlador2);
        btnAtras.setOnClickListener(Controlador2);
        txtTitulo.setOnClickListener(Controlador2);
        txtCont.setOnClickListener(Controlador2);



    }
}
