package utad.actividad1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import utad.libreria.QBAdmin;

public class MainActivity extends AppCompatActivity  {

    Controlador controlador ;
    public Button btnVolver;
    public Button btnEditar;
    public Button btnGuardar;
    public Button btnCancelar;
    public EditText txtNombre;
    public EditText txtEmail;
    public EditText txtTelf;
    public EditText txtDirec;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        controlador = new Controlador(this);

        btnVolver = (Button) this.findViewById(R.id.btnVolver);
        btnVolver.setOnClickListener(controlador);
        btnEditar = (Button) this.findViewById(R.id.btnEditar);
        btnEditar.setOnClickListener(controlador);
        txtNombre = (EditText) this.findViewById(R.id.txtNombre);
        txtNombre.setOnClickListener(controlador);
        txtEmail = (EditText) this.findViewById(R.id.txtEmail);
        txtEmail.setOnClickListener(controlador);
        txtTelf = (EditText) this.findViewById(R.id.txtTelf);
        txtTelf.setOnClickListener(controlador);
        txtDirec = (EditText) this.findViewById(R.id.txtDirec);
        txtDirec.setOnClickListener(controlador);

        txtNombre.setEnabled(false);
        txtEmail.setEnabled(false);
        txtTelf.setEnabled(false);
        txtDirec.setEnabled(false);

        txtNombre.setText(DataHolder.sNombre);
        txtEmail.setText(DataHolder.sEmail);
        txtTelf.setText(DataHolder.sTelf);
        txtDirec.setText(DataHolder.sDirec);
    }

}



