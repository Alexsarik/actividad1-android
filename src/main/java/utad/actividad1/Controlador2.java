package utad.actividad1;

import android.content.Intent;
import android.view.View;

/**
 * Created by alexander.sari on 22/11/2016.
 */

public class Controlador2 implements View.OnClickListener {
    SegundoActivity vista2;

    String titulos[];
    String contenido[];
    int contador = 0;



    public Controlador2(SegundoActivity vista2) {
        this.vista2 = vista2;
        titulos = new String[]{
                vista2.getResources().getString(R.string.titulo1),
                vista2.getResources().getString(R.string.titulo2),
                vista2.getResources().getString(R.string.titulo3)
        };
        contenido = new String[]{
                vista2.getResources().getString(R.string.text1DonQuijote),
                vista2.getResources().getString(R.string.text2DonQuijote),
                vista2.getResources().getString(R.string.text3DonQuijo)
        };
        vista2.txtTitulo.setText(titulos[0]);
        vista2.txtCont.setText(contenido[0]);

    }

    public void onClick(View view) {
        if (view.getId() == vista2.btnPerfil.getId()) {
            Intent intent = new Intent(vista2, MainActivity.class);
            vista2.startActivity(intent);
            vista2.finish();

        }
        if(view.getId() == vista2.btnSig.getId()){

            if(contador < contenido.length-1) {
                contador++;
                vista2.txtCont.setText(contenido[contador]);
                vista2.txtTitulo.setText(titulos[contador]);
            }

        }
        if(view.getId() == vista2.btnAtras.getId()){

            if(contador > 0) {
                contador--;
                vista2.txtCont.setText(contenido[contador]);
                vista2.txtTitulo.setText(titulos[contador]);
            }

        }

    }
}